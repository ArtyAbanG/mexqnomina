package com.example.artyaban.nomina;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import org.ksoap2.serialization.SoapPrimitive;

public class entradaMal extends AppCompatActivity {



    String numEmpleado;
    String TAG = "Response";
    SoapPrimitive resultString;
    String lista;
    String entrada;
    String salida;
    String horas;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada_mal);
        lista = getIntent().getExtras().getString("lista");
        entrada = getIntent().getExtras().getString("entrada");
        salida = getIntent().getExtras().getString("salida");
        horas = getIntent().getExtras().getString("horas");
        numEmpleado = getIntent().getExtras().getString("numEmpleado");
    }


    public void otro(View view)
    {
        Intent intent = new Intent(entradaMal.this, scanner2.class);
        intent.putExtra("lista",lista);
        intent.putExtra("entrada",entrada);
        intent.putExtra("salida",salida);
        intent.putExtra("horas",horas);
        startActivity(intent);


    }

    public void salir(View view )
    {
        Intent intent = new Intent(entradaMal.this,webview3.class);
        startActivity(intent);

    }
}
