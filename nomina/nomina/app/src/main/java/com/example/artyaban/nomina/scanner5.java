package com.example.artyaban.nomina;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class scanner5 extends AppCompatActivity {



    String lista;
    String idLider;
    String inspector;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lista = getIntent().getExtras().getString("lista");
        idLider = getIntent().getExtras().getString("idLider");

        setContentView(R.layout.activity_scanner3); IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();



    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //Se obtiene el resultado del proceso de scaneo y se parsea
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {


            //Quiere decir que se obtuvo resultado pro lo tanto:
            //Desplegamos en pantalla el contenido del código de barra scaneado
            String scanContent = scanningResult.getContents();


            //Desplegamos en pantalla el nombre del formato del código de barra scaneado

            String scanFormat = scanningResult.getFormatName();




            Intent intent2 = new Intent(scanner5.this,registroComisionMedia.class);
            intent2.putExtra("reporte",scanContent);
            intent2.putExtra("lista",lista);
            intent2.putExtra("idLider",idLider);


            startActivity(intent2);


        }else{

            //Quiere decir que NO se obtuvo resultado
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No se ha recibido datos del scaneo!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
