package com.example.artyaban.nomina;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class recorridoMal extends AppCompatActivity {


    String idLider;
    String idLista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorrido_mal);
        idLider=getIntent().getExtras().getString("lider");
        idLista= getIntent().getExtras().getString("lista");
    }
    public void reintentar(View view )
    {
        Intent intent = new Intent(recorridoMal.this, scanner3.class);
        intent.putExtra("idLider",idLider);
        intent.putExtra("lista",idLista);
        startActivity(intent);

    }
    public void salir (View view )
    {
        Intent intent = new Intent(recorridoMal.this, webView4.class);
        startActivity(intent);
    }
}
