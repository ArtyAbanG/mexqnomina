package com.example.artyaban.nomina;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class webView6 extends AppCompatActivity {
    private WebView explorador;
    public static String bandera;
    public static String idLista;
    public static String idLider;
    public static String listaN;
    public static String entradaN;
    public static String salidaN;
    public static String horasN;
    public static String liderN;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view4);

        setContentView(R.layout.activity_inicio);
        if (isOnlineNet() == true) {
            explorador = (WebView) findViewById(R.id.webView);
            WebSettings webSettings = explorador.getSettings();
            webSettings.setSavePassword(false);
            webSettings.setSaveFormData(false);
            webSettings.setSavePassword(true);
            webSettings.setSaveFormData(true);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);


            webSettings.setJavaScriptEnabled(true);
            explorador.addJavascriptInterface(new WebViewJavaScriptInterface(this), "app");
            explorador.setWebViewClient(new WebViewClient());
            explorador.setWebChromeClient(new WebChromeClient());

            explorador.loadUrl("http://187.188.159.205:8090/appnominaandroid/reportesInicio.aspx?lider=" + inicio.idLider);
            explorador.setWebViewClient(new WebViewClient() {
                // evita que los enlaces se abran fuera nuestra app en el navegador de android
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }

            });


        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(webView6.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("NO TIENES INTERNET");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "REINTENTAR",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent cambiar = new Intent(getApplicationContext(), webView5.class);
                            startActivityForResult(cambiar, 0);
                        }
                    });
            alertDialog.show();


        }
    }






    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 3 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }






    public class WebViewJavaScriptInterface {

        private Context context;

        /*
         * Need a reference to the context in order to sent a post message
         */
        public WebViewJavaScriptInterface(Context context) {
            this.context = context;
        }

        /*
         * This method can be called from Android. @JavascriptInterface
         * required after SDK version 17.
         */
        @JavascriptInterface
        public void makeToast(String message, String lider,boolean lengthLong) {
            inicio.idLider=lider; inicio.bandera= bandera;
            liderN=lider;
            idLista=message;
            idLider=lider;

            Intent cambiar = new Intent(getApplicationContext(), scanner.class);

            startActivityForResult(cambiar, 0);
        }

        @JavascriptInterface
        public void makeToast2(String lista, String entrada,String salida , String horas,String lider) {
            inicio.idLider=lider;
            liderN=lider;
            listaN=lista;
            entradaN=entrada;
            salidaN=salida;
            horasN=horas;
            Intent cambiar = new Intent(getApplicationContext(), scanner2.class);
            cambiar.putExtra("lista",listaN);
            cambiar.putExtra("entrada",entradaN);
            cambiar.putExtra("salida",salidaN);
            cambiar.putExtra("horas",horasN);


            startActivityForResult(cambiar, 0);

        }
        @JavascriptInterface
        public void makeToast3(String lista,  String Lider) {
            inicio.idLider=Lider;
            liderN=Lider;
            listaN=lista;
            liderN = Lider;
            Intent cambiar = new Intent(getApplicationContext(), scanner3.class);
            cambiar.putExtra("lista",listaN);
            cambiar.putExtra("idLider",liderN);
            startActivityForResult(cambiar, 0);

        }

        @JavascriptInterface
        public void makeToast4(String lista,  String Lider, String inspector) {
            inicio.idLider=Lider;
            liderN=Lider;

            listaN=lista;
            liderN = Lider;
            Intent cambiar = new Intent(getApplicationContext(), scanner4.class);
            cambiar.putExtra("lista",listaN);
            cambiar.putExtra("idLider",liderN);
            cambiar.putExtra("inspector",inspector);

            startActivityForResult(cambiar, 0);

        }




        @JavascriptInterface
        public void makeToast5(String lista,  String Lider) {
            inicio.idLider=Lider;
            liderN=Lider;

            listaN=lista;
            liderN = Lider;
            Intent cambiar = new Intent(getApplicationContext(), scanner5.class);
            cambiar.putExtra("lista",listaN);
            cambiar.putExtra("idLider",liderN);


            startActivityForResult(cambiar, 0);

        }







    }
}



