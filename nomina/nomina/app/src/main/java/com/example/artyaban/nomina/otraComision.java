package com.example.artyaban.nomina;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class otraComision extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otra_comision);
    }


    public void si(View view)
    {
        Intent intent = new Intent(otraComision.this, scanner.class);
        startActivity(intent);

    }

    public void no(View view)
    {
        Intent intent = new Intent(otraComision.this, webView2.class);
        startActivity(intent);

    }
}
