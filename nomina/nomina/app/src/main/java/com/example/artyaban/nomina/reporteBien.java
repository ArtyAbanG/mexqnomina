package com.example.artyaban.nomina;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class reporteBien extends AppCompatActivity {


    String lista;
    String inspector;
    String lider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte_bien);


        lista = getIntent().getExtras().getString("lista");
        lider = getIntent().getExtras().getString("idLider");

        inspector = getIntent().getExtras().getString("inspector");


    }

    public void otro(View view)
    {


        Intent intent = new Intent(reporteBien.this, scanner4.class);

        intent.putExtra("lista",lista);
        intent.putExtra("inspector",inspector);
        intent.putExtra("idLider",lider);
        startActivity(intent);

    }



    public void inspector(View view )
    {
        Intent intent = new Intent(reporteBien.this, webView5.class);

        intent.putExtra("idLider",lider);
        startActivity(intent);


    }

}
