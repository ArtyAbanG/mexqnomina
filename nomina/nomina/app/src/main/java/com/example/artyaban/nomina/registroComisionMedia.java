package com.example.artyaban.nomina;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class registroComisionMedia extends AppCompatActivity {




    String reporte;
    String TAG = "Response";
    SoapPrimitive resultString;
    String lista;
    String lider;
    String inspector;
    String horas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_comision_media);

        lista = getIntent().getExtras().getString("lista");
        lider = getIntent().getExtras().getString("idLider");
        reporte = getIntent().getExtras().getString("reporte");



    }




    public void registra (View view )
    {
        EditText entrada = (EditText)findViewById(R.id.editText);
        EditText salida = (EditText)findViewById(R.id.editText2);
       String entradaa;
        entradaa = entrada.getText().toString();
        String salidaa;
        salidaa = salida.getText().toString();




            AsyncCallWS task = new AsyncCallWS();
            task.execute();
    }



    private class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "doInBackground");


            registrar();
            return null;
        }

        protected void onPostExecute(Void result) {
            Log.e(TAG, "onPostExecute");
            Log.e(TAG, "result string" +resultString.toString().trim());

            if(resultString.toString().trim().equals("true"))
            {
                Intent intent = new Intent(registroComisionMedia.this, reporteBien.class);

                intent.putExtra("lista",lista);
                intent.putExtra("inspector",inspector);
                intent.putExtra("idLider",lider);
                intent.putExtra("horas",horas);
                intent.putExtra("reporte",reporte);
                startActivity(intent);


            }else
            {
                Intent intent = new Intent(registroComisionMedia.this,reporteMal.class);
                intent.putExtra("lista",lista);
                intent.putExtra("inspector",inspector);
                intent.putExtra("idLider",lider);
                intent.putExtra("horas",horas);
                intent.putExtra("reporte",reporte);
                startActivity(intent);


            }


        }

    }

    public void registrar() {
        String SOAP_ACTION = "http://tempuri.org/comisionMedia";
        String METHOD_NAME = "comisonMedia";
        String NAMESPACE = "http://tempuri.org/";
        String URL = "http://187.188.159.205:8090/webServiceNomina/Service.asmx";

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            Log.e(TAG, "Lista :  "+lista);
            Log.e(TAG, "inspector :  "+inspector);
            Log.e(TAG, "reporte :  "+reporte);
            Log.e(TAG, "horas :  "+horas);
            Log.e(TAG, "lider :  "+lider);

            Request.addProperty("lista", lista);
            Request.addProperty("inspector",inspector);
            Request.addProperty("reporte", reporte);
            Request.addProperty("horas", horas);
            Request.addProperty("idLider",lider);






            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();

            Log.e(TAG, "NO FALLO");

        } catch (Exception ex) {
            Log.e(TAG, "Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: " + ex.getMessage());
        }


    }

    public void regresar(View view )
    {

        Intent intent = new Intent(registroComisionMedia.this, scanner4.class);

        intent.putExtra("lista",lista);
        intent.putExtra("inspector",inspector);
        intent.putExtra("idLider",lider);

        startActivity(intent);


    }



}
