package com.example.artyaban.nomina;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class enviarRecorrido extends AppCompatActivity {

    String lista;
    String numEmpleado;
    String idLider;
    String TAG = "Response";
    SoapPrimitive resultString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_recorrido);

        lista = getIntent().getExtras().getString("lista");
        idLider = getIntent().getExtras().getString("idLider");
        numEmpleado = getIntent().getExtras().getString("numEmpleado");
        AsyncCallWS task = new AsyncCallWS();
        task.execute();
    }


    private class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "doInBackground");


            registrar();
            return null;
        }

        protected void onPostExecute(Void result) {
            Log.i(TAG, "onPostExecute");

            if(resultString.toString().trim().equals("true"))
            {
                Intent intent = new Intent(enviarRecorrido.this, recorridoBien.class);

                intent.putExtra("lista",lista);
                intent.putExtra("lider",idLider);

                startActivity(intent);


            }else
            {
                Intent intent = new Intent(enviarRecorrido.this, recorridoMal.class);
                intent.putExtra("lista",lista);
                intent.putExtra("lider",idLider);

                startActivity(intent);


            }


        }

    }

    public void registrar() {
        String SOAP_ACTION = "http://tempuri.org/recorrido";
        String METHOD_NAME = "recorrido";
        String NAMESPACE = "http://tempuri.org/";
        String URL = "http://187.188.159.205:8090/webServiceNomina/Service.asmx";

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            Log.e(TAG, "idLIsta: " + lista);
            Log.e(TAG, "lider :  "+ idLider);

            Log.e(TAG, " numEmpleado :  " + numEmpleado);

            Request.addProperty("idLista", lista);
            Request.addProperty("numEmpleado", numEmpleado);
           Request.addProperty("lider",idLider);






            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();

            Log.e(TAG, "Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: ");

        } catch (Exception ex) {
            Log.e(TAG, "Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr2rrrr: " + ex.getMessage());
        }


    }

}
