package com.example.artyaban.nomina;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class reporteMal extends AppCompatActivity {


    String lista;
    String lider;
    String reporte;
    String inspector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte_mal);

        lista = getIntent().getExtras().getString("lista");
        lider = getIntent().getExtras().getString("idLider");
        reporte = getIntent().getExtras().getString("reporte");
        inspector = getIntent().getExtras().getString("inspector");

    }

    public void reintentar(View view )
    {


        Intent intent = new Intent(reporteMal.this, scanner4.class);

        intent.putExtra("lista",lista);
        intent.putExtra("inspector",inspector);
        intent.putExtra("idLider",lider);
        startActivity(intent);

    }

    public void salir(View view)
    {Intent intent = new Intent(reporteMal.this, webView5.class);

        intent.putExtra("idLider",lider);
        startActivity(intent);
    }
}
