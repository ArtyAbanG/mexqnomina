package com.example.artyaban.nomina;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class enviarEntrada extends AppCompatActivity {

    String numEmpleado;
    String TAG = "Response";
    SoapPrimitive resultString;
    String lista;
    String entrada;
    String salida;
    String horas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_entrada);


        lista = getIntent().getExtras().getString("lista");
        entrada = getIntent().getExtras().getString("entrada");
        salida = getIntent().getExtras().getString("salida");
        horas = getIntent().getExtras().getString("horas");
        numEmpleado = getIntent().getExtras().getString("numEmpleado");
        AsyncCallWS task = new AsyncCallWS();
        task.execute();
    }


    private class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "doInBackground");


            registrar();
            return null;
        }

        protected void onPostExecute(Void result) {
            Log.i(TAG, "onPostExecute");

            if(resultString.toString().trim().equals("true"))
            {
                Intent intent = new Intent(enviarEntrada.this, entradaBien.class);

                intent.putExtra("lista",lista);
                intent.putExtra("entrada",entrada);
                intent.putExtra("salida",salida);
                intent.putExtra("horas",horas);
                startActivity(intent);


            }else
            {
                Intent intent = new Intent(enviarEntrada.this, entradaMal.class);
                intent.putExtra("lista",lista);
                intent.putExtra("entrada",entrada);
                intent.putExtra("salida",salida);
                intent.putExtra("horas",horas);
                startActivity(intent);


            }


        }

    }

    public void registrar() {
        String SOAP_ACTION = "http://tempuri.org/Entrada";
        String METHOD_NAME = "Entrada";
        String NAMESPACE = "http://tempuri.org/";
        String URL = "http://187.188.159.205:8090/webServiceNomina/Service.asmx";

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);

            Request.addProperty("idLista", lista);
            Request.addProperty("entrada", entrada);
            Request.addProperty("salida", salida);
            Request.addProperty("numEmpleado", numEmpleado);
            Request.addProperty("horas", horas);
            Request.addProperty("incidencia", "Asistencia");






            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();

            Log.e(TAG, "Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: ");

        } catch (Exception ex) {
            Log.e(TAG, "Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: " + ex.getMessage());
        }


    }

}
