package com.example.artyaban.nomina;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class segurohorario extends AppCompatActivity {



    String entradaa;
    String salidaa;
    String numEmpleado;
    public static String idLista;
    public static String Horas;
    String TAG = "Response";
    Button bt;
    public static String respuesta;
    String jornadaString;

    String getCel;
    SoapPrimitive resultString;
    String usuarioo  ;
    String passwordd;
    View view2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segurohorario);



        entradaa=getIntent().getExtras().getString("entrada");
        salidaa=getIntent().getExtras().getString("salida");
        numEmpleado=getIntent().getExtras().getString("numEmpleado");
        TextView entrada = (TextView) findViewById(R.id.horaEntrada);
        TextView jornada = (TextView) findViewById(R.id.joranda);
        TextView salida = (TextView) findViewById(R.id.horaSalida);
        TextView empleado = (TextView) findViewById(R.id.empleadd);
        entrada.setText(entradaa);
        salida.setText(salidaa);
        empleado.setText(numEmpleado);
        String tomarHorasEntrada="";
        String tomarHorasSalida="";
        String tomarMinutosEntrada="";
        String tomarMinutosSalida="";

        tomarHorasEntrada = entradaa.substring(0,2);
        tomarMinutosEntrada = entradaa.substring(3,5);
        tomarHorasSalida = salidaa.substring(0,2);
        tomarMinutosSalida = salidaa.substring(3,5);


        String TAG = "Response";
        Log.i(TAG, "HORAS TOAMDAS EN STRING");
        Log.i(TAG,tomarHorasEntrada);
        Log.i(TAG,tomarMinutosEntrada);
        Log.i(TAG,tomarHorasSalida);
        Log.i(TAG,tomarMinutosSalida);


        int HorasEntrada = Integer.parseInt(tomarHorasEntrada);
        int MinutosEntrada = Integer.parseInt(tomarMinutosEntrada);
        int tiempoMinutosEntrada;

        tiempoMinutosEntrada= (HorasEntrada*60)+MinutosEntrada;

        Log.i(TAG, "MINUTOS ENTRADA INT");
        Log.i(TAG, Integer.toString(tiempoMinutosEntrada));

        int HorasSalida = Integer.parseInt(tomarHorasSalida);
        int MinutosSalida = Integer.parseInt(tomarMinutosSalida);
        int tiempoMinutosSalida;

        tiempoMinutosSalida= (HorasSalida*60)+MinutosSalida;

        Log.i(TAG, "MINUTOS SALIDA INT");
        Log.i(TAG,Integer.toString(tiempoMinutosSalida));

        int v1= 720 - tiempoMinutosEntrada;
        int v2 = v1+ tiempoMinutosSalida;
        int v3=0;

        if(v2>720) {
            v3 = v2 - 720;
        }else{v3=v2;}

        int totaljornada = v3/60;
        jornadaString = Integer.toString(totaljornada);
        jornada.setText(jornadaString);



    }






    public void  si(View view) {
        AsyncCallWS task = new AsyncCallWS();
        task.execute();
    }

    public void  no(View view)
    {
        Intent intent = new Intent(segurohorario.this, scanner.class);
        startActivity(intent);

    }



    private class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "doInBackground");


            registrar();
            return null;
        }

        protected void onPostExecute(Void result) {Log.i(TAG, "onPostExecute");

            try {
                Log.e(TAG, "resultado del web serviceeeee: " + respuesta.trim());
                if (respuesta.trim().equals("true")) {
                    Intent intent = new Intent(segurohorario.this, otraComision.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(segurohorario.this, repetido.class);
                    startActivity(intent);

                }
            }catch(Exception ex)
            {
                Intent intent = new Intent(segurohorario.this, repetido.class);
                startActivity(intent);
            }




    }

    }

    public void registrar() {
        String SOAP_ACTION = "http://tempuri.org/comision";
        String METHOD_NAME = "comision";
        String NAMESPACE = "http://tempuri.org/";
        String URL = "http://187.188.159.205:8090/webServiceNomina/Service.asmx";

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            Log.e(TAG, "idLista: " + inicio.idLista);
            Log.e(TAG, "Entrada: " + entradaa);
            Log.e(TAG, "salida: " + salidaa);
            Log.e(TAG, "numEmpleado: " + numEmpleado);
            Log.e(TAG, "Horas: " + jornadaString);

            Request.addProperty("idLista",inicio.idLista);
            Request.addProperty("entrada",entradaa);
            Request.addProperty("salida",salidaa);
            Request.addProperty("numEmpleado",numEmpleado);
            Request.addProperty("horas",jornadaString);




            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            respuesta= resultString.toString();
            Log.e(TAG, "Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: ");

        } catch (Exception ex) {
            Log.e(TAG, "Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: " + ex.getMessage());
        }
    }






}
