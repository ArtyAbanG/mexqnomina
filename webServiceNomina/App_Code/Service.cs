﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class Service : System.Web.Services.WebService
{
    public Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    [WebMethod]
    public string registro2(int idLista,string entrada,string salida, string numEmpleado,int horas)
    {
        if (numEmpleado == null) { return "false"; }
        if (numEmpleado == "null") { return "false"; }
        if (numEmpleado == "") { return "false"; }
        if (idLista ==null) { return "false"; }

        
        string query2 = "insert into RegistrosAppNomina(idLista,entrada,salida,horas,empleado)values(@idLista,@entrada,@salida,@horas,@numEmpleado)";
        //cadena de conexion  
        SqlConnection cnx = new SqlConnection("Data Source=MEXQ-SERVER4;Initial Catalog=MEXQAppPr;User ID=sa;Password=P@ssw0rd");
        SqlCommand cmx = new SqlCommand();
        cmx.Connection = cnx;
        cmx.CommandText = query2;
        //enviamos los párametros
        cmx.Parameters.AddWithValue("@idLista", idLista);
         cmx.Parameters.AddWithValue("@entrada",entrada);
             cmx.Parameters.AddWithValue("@salida",salida);
             cmx.Parameters.AddWithValue("@numEmpleado",numEmpleado);
             cmx.Parameters.AddWithValue("@horas", horas);
  
        SqlDataReader dr2;
        try
        {
            //abre la conexion
            cnx.Open();
            //devuelve la fila afectada
            dr2 = cmx.ExecuteReader();
            cnx.Close();
       
           return "true";
          
        }
        catch (SqlException ex)
        {
            return "false";
        }

    }


    [WebMethod]
    public string crearLista(string lider)
    {

    
        string query2 = "insert into ListasAppNomina(lider,status)values(@lider,'abierta')";
        //cadena de conexion  
        SqlConnection cnx = new SqlConnection("Data Source=MEXQ-SERVER4;Initial Catalog=MEXQAppPr;User ID=sa;Password=P@ssw0rd");
        SqlCommand cmx = new SqlCommand();
        cmx.Connection = cnx;
        cmx.CommandText = query2;
        //enviamos los párametros
        cmx.Parameters.AddWithValue("@lider", lider);
  
        SqlDataReader dr2;
        try
        {
            //abre la conexion
            cnx.Open();
            //devuelve la fila afectada
            dr2 = cmx.ExecuteReader();
            cnx.Close();
       
           
          
        }
        catch (SqlException ex)
        {
            return "false";
        }



        string query23 = "select max(id) as id from ListasAppNomina where lider=@lider";
        //cadena de conexion  
        SqlConnection scnx31 = new SqlConnection("Data Source=MEXQ-SERVER4;Initial Catalog=MEXQAppPr;User ID=sa;Password=P@ssw0rd");
        SqlCommand cmd1 = new SqlCommand();
        cmd1.Parameters.AddWithValue("@lider", lider);
  
        cmd1.Connection = scnx31;
        cmd1.CommandText = query23;
        //enviamos los párametros
      

        SqlDataReader dr23;
        try
        {
            //abre la conexion
            scnx31.Open();
            //devuelve la fila afectada
            dr23 = cmd1.ExecuteReader();

            dr23.Read();

            string id = dr23["id"].ToString();
            scnx31.Close(); 
            return id;


        }
        catch (SqlException ex)
        {
            return "false";
        }
 }





    [WebMethod]
    public string comision(string idLista,string entrada,string salida, string numEmpleado,string horas)
    {

        if (numEmpleado == null) { return "fals no empleadoe"; }
        if (numEmpleado == "null") { return "false no empleado"; }
        if (numEmpleado == "") { return "false no empleado"; }
        if (idLista ==null) { return "false no lista"; }

      
        string query21 = "select * from RegistrosAppNomina where idLista = @idLista";
        //cadena de conexion  
        SqlConnection cnx1 = new SqlConnection("Data Source=MEXQ-SERVER4;Initial Catalog=MEXQAppPr;User ID=sa;Password=P@ssw0rd");
        SqlCommand cmx1 = new SqlCommand();
        cmx1.Connection = cnx1;
        cmx1.CommandText = query21;
        //enviamos los párametros
        cmx1.Parameters.AddWithValue("@idLista", idLista);
        cnx1.Open();
  
        SqlDataReader dr21;
        try
        {
            //abre la conexion
           
            //devuelve la fila afectada
            dr21 = cmx1.ExecuteReader();
          
            try
            {
                while (dr21.Read())
                {
                    if (dr21["empleado"].ToString().Trim().Equals(numEmpleado.Trim()))
                    { return "false repetido"; }
                    else {  }
                }

                cnx1.Close();

            }
            catch (SqlException ex) { cnx1.Close(); }
       
         
          
        }
        catch (SqlException ex)
        {
            return "false fallo sql";
        }

        
        string query2 = "insert into RegistrosAppNomina(idLista,entrada,salida,horas,empleado,incidenciaEntrada)values(@idLista,@entrada,@salida,@horas,@numEmpleado,'comision prestamo Entrada')";
        //cadena de conexion  
        SqlConnection cnx = new SqlConnection("Data Source=MEXQ-SERVER4;Initial Catalog=MEXQAppPr;User ID=sa;Password=P@ssw0rd");
        SqlCommand cmx = new SqlCommand();
        cmx.Connection = cnx;
        cmx.CommandText = query2;
        //enviamos los párametros
        cmx.Parameters.AddWithValue("@idLista", idLista);
         cmx.Parameters.AddWithValue("@entrada",entrada);
             cmx.Parameters.AddWithValue("@salida",salida);
             cmx.Parameters.AddWithValue("@numEmpleado",numEmpleado);
             cmx.Parameters.AddWithValue("@horas", horas);
  
        SqlDataReader dr2;
        try
        {
            //abre la conexion
            cnx.Open();
            //devuelve la fila afectada
            dr2 = cmx.ExecuteReader();
            cnx.Close();
       
           return "true";
          
        }
        catch (SqlException ex)
        {
            return "false fallo sql insert";
        }

    }














    [WebMethod]
    public string Entrada(string idLista, string entrada, string salida, string numEmpleado, string horas,string incidencia)
    {

        if (numEmpleado == null) { return "fals no empleadoe"; }
        if (numEmpleado == "null") { return "false no empleado"; }
        if (numEmpleado == "") { return "false no empleado"; }
        if (idLista == null) { return "false no lista"; }


        string query21 = "select * from RegistrosAppNomina where idLista = @idLista";
        //cadena de conexion  
        SqlConnection cnx1 = new SqlConnection("Data Source=MEXQ-SERVER4;Initial Catalog=MEXQAppPr;User ID=sa;Password=P@ssw0rd");
        SqlCommand cmx1 = new SqlCommand();
        cmx1.Connection = cnx1;
        cmx1.CommandText = query21;
        //enviamos los párametros
        cmx1.Parameters.AddWithValue("@idLista", idLista);
        cnx1.Open();

        SqlDataReader dr21;
        try
        {
            //abre la conexion

            //devuelve la fila afectada
            dr21 = cmx1.ExecuteReader();

            try
            {
                while (dr21.Read())
                {
                    if (dr21["empleado"].ToString().Trim().Equals(numEmpleado.Trim()))
                    { return "false repetido"; }
                    else { }
                }

                cnx1.Close();

            }
            catch (SqlException ex) { cnx1.Close(); }



        }
        catch (SqlException ex)
        {
            return "false fallo sql";
        }


        string query2 = "insert into RegistrosAppNomina(idLista,entrada,salida,horas,empleado,incidenciaEntrada)values(@idLista,@entrada,@salida,@horas,@numEmpleado,@incidencia)";
        //cadena de conexion  
        SqlConnection cnx = new SqlConnection("Data Source=MEXQ-SERVER4;Initial Catalog=MEXQAppPr;User ID=sa;Password=P@ssw0rd");
        SqlCommand cmx = new SqlCommand();
        cmx.Connection = cnx;
        cmx.CommandText = query2;
        //enviamos los párametros
        cmx.Parameters.AddWithValue("@idLista", idLista);
        cmx.Parameters.AddWithValue("@entrada", entrada);
        cmx.Parameters.AddWithValue("@salida", salida);
        cmx.Parameters.AddWithValue("@numEmpleado", numEmpleado);
        cmx.Parameters.AddWithValue("@horas", horas);
        cmx.Parameters.AddWithValue("@incidencia",incidencia);

        SqlDataReader dr2;
        try
        {
            //abre la conexion
            cnx.Open();
            //devuelve la fila afectada
            dr2 = cmx.ExecuteReader();
            cnx.Close();

            return "true";

        }
        catch (SqlException ex)
        {
            return "false fallo sql insert";
        }

    }









    }

